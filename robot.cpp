
#define PROGRAM_TITLE "Project"
#define DISPLAY_INFO "Tim Rotzinger"

#define SIZE 512

#include <stdlib.h>  // Useful for the following includes.
#include <stdio.h>    
#include <string.h>  // For spring operations.
#include <time.h>

#include <GL/gl.h>   // OpenGL itself.
#include <GL/glu.h>  // GLU support library.
#include <GL/glut.h> // GLUT support library.
#include <GL/glut.h>

struct Coord
{
   float x;
   float y;
   float z;
};

Coord buildings[20][20];

// Some global variables.
// Window IDs, window width and height.
int Window_ID;
int Window_Width = 800;
int Window_Height = 600;

// Cube position and rotation speed variables.
float X_Rot   = 0.9f;  
float Y_Rot   = .9f;  
float C_Speed = 0.5f;  
float P_Speed = 0.5f;
float S_Speed = 0.5f;
float Z_Off   =-5.0f;
float rotate = 0;
float cube_Rot = .9f;
float pyramid_Rot = .9f;
float sphere_Rot = .9f;
float robotPosition[] = {3.5,-.25,-.5};
float lookx = robotPosition[0];
float looky = robotPosition[1];
float lookz = robotPosition[2] + 5;
float antRot = 30.0f;
float head_Rot = 180.0f;
float head_Rot_Speed = 0.3f;

bool pause = false;

// globals for vectors
int p1[] = {0,0,0};
int p2[] = {3,3,3};

//global for cube center
int c[] = {1,1,1};

//initialize random seed


//////////////////////////////////////////////////////////
// String rendering routine; leverages on GLUT routine. //
//////////////////////////////////////////////////////////
static void PrintString(void *font, char *str)
{
   int i,len=strlen(str);

   for(i=0;i < len; i++){
      glutBitmapCharacter(font,*str++);
   }
}
void renderBuilding(float x, float y, float z)
{  
   glPushMatrix();
   glTranslatef(x,y,z);
   glScalef(0.5, 0.5, 0.5);
   
   // Render a building
   glBegin( GL_QUADS );
        // Top face
       glColor3f(  (0.0f), (0.85f),  (0.15f) ); 

       glVertex3f(  (+1.0f), (+1.0f),
    		(-1.0f) );  // Top-right of top face
       glVertex3f( (-1.0f), (+1.0f), (-1.0f) );  // Top-left of top face
       glVertex3f( (-1.0f), (+1.0f),
    	       (+1.0f) );  // Bottom-left of top face
       glVertex3f(  (+1.0f), (+1.0f),
    		(+1.0f) );  // Bottom-right of top face
     
        // Bottom face
       glColor3f(  1.0f,  0.5f,  0.0f ); // Orange

       glVertex3f(  (+1.0f), (-1.0f),
    		(-1.0f) ); // Top-right of bottom face
       glVertex3f( (-1.0f), (-1.0f),
    	       (-1.0f) ); // Top-left of bottom face
       glVertex3f( (-1.0f), (-1.0f),
    	       (+1.0f) ); // Bottom-left of bottom face
       glVertex3f(  (+1.0f), (-1.0f),
    		(+1.0f) ); // Bottom-right of bottom face
         // Front face
       glColor3f(   1.0f,  0.0f, 0.1f );  // Red

       glVertex3f(  (+1.0f),  +1.0f,
    		+1.0f );  // Top-Right of front face
       glVertex3f( (-1.0f),  +1.0f,
    	       +1.0f );  // Top-left of front face
       glVertex3f( -1.0f, -1.0f,
    	       +1.0f );  // Bottom-left of front face
       glVertex3f( + 1.0f, -1.0f,
    	       +1.0f );  // Bottom-right of front face
     
        // Back face
       glColor3f(   1.0f,  1.0f,  0.0f ); // Yellow

       glVertex3f( + 1.0f, -1.0f,
    	       -1.0f ); // Bottom-Left of back face
       glVertex3f( -1.0f, -1.0f,
    	       -1.0f ); // Bottom-Right of back face
       glVertex3f( -1.0f,  +1.0f,
    	       -1.0f ); // Top-Right of back face
       glVertex3f( + 1.0f,  +1.0f,
    	       -1.0f ); // Top-Left of back face
     
        // Left face
       glColor3f(   0.0f,  0.0f,  1.0f);  // Blue

       glVertex3f( -1.0f,  +1.0f,
    	       +1.0f);  // Top-Right of left face
       glVertex3f( -1.0f,  +1.0f,
    	       -1.0f);  // Top-Left of left face
       glVertex3f( -1.0f, -1.0f,
    	       -1.0f);  // Bottom-Left of left face
       glVertex3f( -1.0f, -1.0f,
    	       +1.0f);  // Bottom-Right of left face
         // Right face
       glColor3f(  1.0f,  0.0f,  1.0f);  // Violet

       glVertex3f( + 1.0f,  +1.0f,
    	       +1.0f);  // Top-Right of left face
       glVertex3f( + 1.0f,  +1.0f,
    	       -1.0f);  // Top-Left of left face
       glVertex3f(  +1.0f, -1.0f,
    		-1.0f);  // Bottom-Left of left face
       glVertex3f(  1.0f, -1.0f,
    		1.0f);  // Bottom-Right of left face
   glEnd();
   glPopMatrix();
}//end render building************
void renderRobot()
{
   glPushMatrix();
   //glMatrixMode(GL_MODELVIEW);
   // glLoadIdentity();
   glScalef(.5,.8,.5);
   // Render a cube body
   glBegin( GL_QUADS );
   // Top face
   glColor3f(  (0.0f), (0.85f),  (0.15f) ); 

   glVertex3f(  (+1.0f), (+1.0f),
		(-1.0f) );  // Top-right of top face
   glVertex3f( (-1.0f), (+1.0f), (-1.0f) );  // Top-left of top face
   glVertex3f( (-1.0f), (+1.0f),
	       (+1.0f) );  // Bottom-left of top face
   glVertex3f(  (+1.0f), (+1.0f),
		(+1.0f) );  // Bottom-right of top face
 
   // Bottom face
   glColor3f(  1.0f,  0.5f,  0.0f ); // Orange

   glVertex3f(  (+1.0f), (-1.0f),
		(-1.0f) ); // Top-right of bottom face
   glVertex3f( (-1.0f), (-1.0f),
	       (-1.0f) ); // Top-left of bottom face
   glVertex3f( (-1.0f), (-1.0f),
	       (+1.0f) ); // Bottom-left of bottom face
   glVertex3f(  (+1.0f), (-1.0f),
		(+1.0f) ); // Bottom-right of bottom face
   // Front face
   glColor3f(   1.0f,  0.0f, 0.1f );  // Red

   glVertex3f(  (+1.0f),  +1.0f,
		+1.0f );  // Top-Right of front face
   glVertex3f( (-1.0f),  +1.0f,
	       +1.0f );  // Top-left of front face
   glVertex3f( -1.0f, -1.0f,
	       +1.0f );  // Bottom-left of front face
   glVertex3f( + 1.0f, -1.0f,
	       +1.0f );  // Bottom-right of front face
 
   // Back face
   glColor3f(   1.0f,  1.0f,  0.0f ); // Yellow

   glVertex3f( + 1.0f, -1.0f,
	       -1.0f ); // Bottom-Left of back face
   glVertex3f( -1.0f, -1.0f,
	       -1.0f ); // Bottom-Right of back face
   glVertex3f( -1.0f,  +1.0f,
	       -1.0f ); // Top-Right of back face
   glVertex3f( + 1.0f,  +1.0f,
	       -1.0f ); // Top-Left of back face
 
   // Left face
   glColor3f(   0.0f,  0.0f,  1.0f);  // Blue

   glVertex3f( -1.0f,  +1.0f,
	       +1.0f);  // Top-Right of left face
   glVertex3f( -1.0f,  +1.0f,
	       -1.0f);  // Top-Left of left face
   glVertex3f( -1.0f, -1.0f,
	       -1.0f);  // Bottom-Left of left face
   glVertex3f( -1.0f, -1.0f,
	       +1.0f);  // Bottom-Right of left face
   // Right face
   glColor3f(  1.0f,  0.0f,  1.0f);  // Violet

   glVertex3f( + 1.0f,  +1.0f,
	       +1.0f);  // Top-Right of left face
   glVertex3f( + 1.0f,  +1.0f,
	       -1.0f);  // Top-Left of left face
   glVertex3f(  +1.0f, -1.0f,
		-1.0f);  // Bottom-Left of left face
   glVertex3f(  1.0f, -1.0f,
		1.0f);  // Bottom-Right of left face
   glEnd();
   // add design to robot's back
   glColor3f(.33,0,1);
   glTranslatef(-.25,.25,2.1);
   glBegin(GL_POLYGON);
   glVertex3f(.5,.5,-1.05);
   glVertex3f(0,.5,-1.05);
   glVertex3f(0,0,-1.05);
   glVertex3f(.15,0,-1.05);
   glVertex3f(.15,.45,-1.05);
   glVertex3f(.20,.45,-1.05);
   glVertex3f(.2,0,-1.05);
   glVertex3f(.25,0,-1.05);
   glVertex3f(.25,.45,-1.05);
   glVertex3f(.30,.45,-1.05);
   glVertex3f(.30,0,-1.05);
   glVertex3f(.35,0,-1.05);
   glVertex3f(.35,.45,-1.05);
   glVertex3f(.40,.45,-1.05);
   glVertex3f(.40,0,-1.05);
   glVertex3f(.5,0,-1.05);
   glVertex3f(.5,.5,-1.05);
   glEnd();
   glTranslatef(.25,-.25,-2.1);
      
   // Render a cube head************************************************
   // glLoadIdentity();
   glRotatef(head_Rot,0,1,0); 
   glScalef(.5,.5,.5);
   glTranslatef(0,2,0);
   glBegin( GL_QUADS );
   // Top face
   glColor3f(  (0.0f), (0.85f),  (0.15f) ); 

   glVertex3f(  (+1.0f), (+1.0f),
		(-1.0f) );  // Top-right of top face
   glVertex3f( (-1.0f), (+1.0f), (-1.0f) );  // Top-left of top face
   glVertex3f( (-1.0f), (+1.0f),
	       (+1.0f) );  // Bottom-left of top face
   glVertex3f(  (+1.0f), (+1.0f),
		(+1.0f) );  // Bottom-right of top face
 
   // Bottom face
   glColor3f(  1.0f,  0.5f,  0.0f ); // Orange

   glVertex3f(  (+1.0f), (-1.0f),
		(-1.0f) ); // Top-right of bottom face
   glVertex3f( (-1.0f), (-1.0f),
	       (-1.0f) ); // Top-left of bottom face
   glVertex3f( (-1.0f), (-1.0f),
	       (+1.0f) ); // Bottom-left of bottom face
   glVertex3f(  (+1.0f), (-1.0f),
		(+1.0f) ); // Bottom-right of bottom face
   // Front face
   glColor3f(   1.0f,  0.0f, 0.1f );  // Red

   glVertex3f(  (+1.0f),  +1.0f,
		+1.0f );  // Top-Right of front face
   glVertex3f( (-1.0f),  +1.0f,
	       +1.0f );  // Top-left of front face
   glVertex3f( -1.0f, -1.0f,
	       +1.0f );  // Bottom-left of front face
   glVertex3f( + 1.0f, -1.0f,
	       +1.0f );  // Bottom-right of front face
 
   // Back face
   glColor3f(   1.0f,  1.0f,  0.0f ); // Yellow

   glVertex3f( + 1.0f, -1.0f,
	       -1.0f ); // Bottom-Left of back face
   glVertex3f( -1.0f, -1.0f,
	       -1.0f ); // Bottom-Right of back face
   glVertex3f( -1.0f,  +1.0f,
	       -1.0f ); // Top-Right of back face
   glVertex3f( + 1.0f,  +1.0f,
	       -1.0f ); // Top-Left of back face
 
   // Left face
   glColor3f(   0.0f,  0.0f,  1.0f);  // Blue

   glVertex3f( -1.0f,  +1.0f,
	       +1.0f);  // Top-Right of left face
   glVertex3f( -1.0f,  +1.0f,
	       -1.0f);  // Top-Left of left face
   glVertex3f( -1.0f, -1.0f,
	       -1.0f);  // Bottom-Left of left face
   glVertex3f( -1.0f, -1.0f,
	       +1.0f);  // Bottom-Right of left face
   // Right face
   glColor3f(  1.0f,  0.0f,  1.0f);  // Violet

   glVertex3f( + 1.0f,  +1.0f,
	       +1.0f);  // Top-Right of left face
   glVertex3f( + 1.0f,  +1.0f,
	       -1.0f);  // Top-Left of left face
   glVertex3f(  +1.0f, -1.0f,
		-1.0f);  // Bottom-Left of left face
   glVertex3f(  1.0f, -1.0f,
		1.0f);  // Bottom-Right of left face
   glEnd();
   
   //add eyes to head
   GLUquadric *quad;
   quad = gluNewQuadric();
   double base = .30;
   double top = .30;
   double height = .5;
   int slice = 100;
   int stacks = 50;
   glColor3f(1,1,1);
   glTranslatef(.5,0.5,1.1);
   gluDisk(quad,0,.3,slice,1);
   glTranslatef(-1,0,0);
   gluDisk(quad,0,.3,slice,1);
   //render antena
   glColor3f(0.0f,1.0f,0.3f);
   glTranslatef(.5,.92,-1.1);
   glRotatef(90,1,0,0);
   glRotatef(antRot,0,0,1);
  
   gluCylinder(quad,base,top,height,slice,stacks);
   
   glColor3f(0.9f,.05f,0.05f);
   glScalef(-1,-1.3,-1);
   glRotatef(30,0,1,0);
   double rad = 0.30;
   slice = 50;
   stacks = 25;
   gluSphere(quad,rad,slice,stacks);
 
   //rotation of antena
    antRot += 30;
   // head_Rot = 0;
   // head_Rot_Speed = 0;
   glPopMatrix();
}// end render robot

void renderGrid()
{

   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();

   float roadOffset = -0.9;
   
   glColor3f(0, 0, 0); //set to black
   //Make Vertical Roads
   glBegin(GL_QUADS);
      for (int i=1; i<20; i++)
      {
         glVertex3f((4*i)-1, roadOffset, 0); //Bottom Left
         glVertex3f(4*i, roadOffset, 0); //Bottom Right
         glVertex3f(4*i, roadOffset, -80); // Top Right
         glVertex3f((4*i)-1, roadOffset, -80); //Top Left
      }
   //Make Horizontal Roads
      for (int i=1; i <20; i++)
      {
         glVertex3f(0, roadOffset, -4*i-1);
         glVertex3f(80, roadOffset, -4*i-1);
         glVertex3f(80, roadOffset, -4*i);
         glVertex3f(0, roadOffset, -4*i);
      }
   glEnd();
   glColor3f(0.1,0.9,0.1); // Green
   
   glBegin(GL_QUADS);
      glVertex3f(0, -1, 0); // Bottom left
      glVertex3f(80, -1, 0); //Bottom Right
      glVertex3f(80, -1, -80); //Top Right
      glVertex3f(0, -1, -80); //Top left
   glEnd();
   glPopMatrix();
}// end render grid

void spawnBuildings()
{
   //For each block 100% chance to spawn a building inside
   
   for (int x = 0; x < 20; ++x)
   {
      for (int z = 0;  z < 20; ++z)
      {
         //Get bottom coordinate of block
         int botX = 4*x;
         int botZ = -4*z;

         int chance = rand() % 1;

         if(chance==0)
         {
            //spawn a building
            buildings[x][z].x = botX+1;
            buildings[x][z].z = botZ-2;
         }
      }
   }
}

void drawBuildings(GLenum mode)
{

   for(int x=0; x<20; ++x)
   {
      for(int z=0; z<20; ++z)
      {
        if (buildings[x][z].x == 0 && buildings[x][z].z == 0)
            continue;
        
        if(mode == GL_SELECT)
        {   
            glLoadName(20*x + z);
        }


        renderBuilding(buildings[x][z].x, -.5, buildings[x][z].z);
      }
   }
}
/////////////////////////////////////////////////////////
// Routine which actually does the drawing             //
/////////////////////////////////////////////////////////
void CallBackRenderScene(void)
{
   char buf[80]; // For our strings.

   // Need to manipulate the ModelView matrix to move our model around.
   glMatrixMode(GL_MODELVIEW);
   glViewport(0,0, Window_Width, Window_Height);
   // Reset to 0,0,0; no rotation, no scaling.
   glLoadIdentity(); 

   // Move the object back from the screen.
   glTranslatef(0.0f,0.0f,Z_Off); 

   gluLookAt(lookx,looky,lookz,robotPosition[0],robotPosition[1],
	     robotPosition[2],0,1,0);
   glPushMatrix(); // save camera
   // Clear the color and depth buffers.
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   renderGrid();
   // renderBuilding(5,0,-8);
   // renderBuilding(-5,0,-8);
   // renderBuilding(-5,0,-15);
   // renderBuilding(2,0,-10);
   drawBuildings(GL_RENDER);
   //rotate robot
   // glRotatef(,0,1,0);
   //update robot position
   glTranslatef(robotPosition[0],robotPosition[1],robotPosition[2]);
   //scale robot
   glScalef(.75,.75,.75);
   // Render a robot
   renderRobot();
   glScalef(.75,.75,.75);
   glTranslatef(-robotPosition[0],-robotPosition[1],-robotPosition[2]);
   // Move the object back from the screen.
   glTranslatef(0.0f,0.0f,Z_Off-4); 

   glPopMatrix();

   glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
   
   // We start with GL_DECAL mode.
   glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
 
 
   // All done drawing.  Let's show it.
   glutSwapBuffers();

   // Now let's do the motion calculations.
   cube_Rot+=C_Speed; 
   pyramid_Rot+=P_Speed;
   sphere_Rot+=S_Speed;
 
}

////////////////////////////////////////////////////////////
// Callback function called when a normal key is pressed. //
////////////////////////////////////////////////////////////
void CallBackKeyPressed(unsigned char key, int x, int y)
{
   if (key == 112 && pause){
      pause = false;
      printf ("unpaused.\n", key);
      return;
   }else if (pause){
      return;
   }
   switch (key) {
      case 122: // z
	 if (robotPosition[2] >= -76 && robotPosition[2] <= 0){
	    robotPosition[2] -= 4;
	    lookz -=  4;
	 }
	 break;
      case 97: // a
	 if (robotPosition[0] >= -1 && robotPosition[0] <= 80){
	    robotPosition[0] += 4;
	    lookx = robotPosition[0];
	 }
	 break;
      case 113: // q
	 if (robotPosition[0] >= -1 && robotPosition[0] <= 80){
	    robotPosition[0] -= 4;
	    lookx = robotPosition[0];
	 }
	 break;
      case 114: //r
	 robotPosition[0] = 3.5;
	 robotPosition[1] = -.25;
	 robotPosition[2] = -.5;
	 lookx = robotPosition[0];
	 looky = robotPosition[1];
	 lookz = robotPosition[2] + 5;
	 break;
      case 112: //p
	 pause = true;
	 printf ("paused.\n", key);
	 break;
   default:
      printf ("KP: No action for %d.\n", key);
      break;
    }
}
void CallBackSpecialPressed(int key, int x, int y)
{
   switch(key){
      case GLUT_KEY_F1:
	 head_Rot = 180;
	 break;
      case GLUT_KEY_F2:
	 head_Rot = 90;
	 break;
      case GLUT_KEY_F3:
	 head_Rot = -90;
	 break;
      case GLUT_KEY_F4:

	 lookx=robotPosition[0];
	 looky=robotPosition[1];
	 lookz=robotPosition[2] + 5;
	 break;
      case GLUT_KEY_F5:

	 lookx = robotPosition[0] - 1;
	 looky = robotPosition[1] + 1;
	 lookz = robotPosition[2] + 2;
	 break;
      case GLUT_KEY_F6:

	 lookx = robotPosition[0] + 1;
	 looky = robotPosition[1] + 1;
	 lookz = robotPosition[2] + 2;
	 break;
      case GLUT_KEY_F7:
	 lookx = robotPosition[0] + 1;
	 looky = robotPosition[1] + 1;
	 lookz = robotPosition[2] - 2;
	 break;
      case GLUT_KEY_F8:
	 lookx = robotPosition[0] - 1;
	 looky = robotPosition[1] + 1;
	 lookz = robotPosition[2] - 2;
	 break;
   }
}
void processHits(GLint hits, GLuint buffer[])
{
    
    printf("hits = %d\n", hits);
    GLint names, *ptr;
    ptr = (GLint *) buffer;

    bool firstHit = false;
    for(int i=0; i < hits; i++)
    {
        names = *ptr;
        ptr += 3;

        printf("names # = %d\n", names);

        if (firstHit)
            break;

        for(int j=0; j < names; j++)
        {
            printf("building # = %d\n", *ptr);

            
            int x = *ptr / 20;
            int z = *ptr % 20;

            buildings[x][z].x = 0;
            buildings[x][z].z = 0;

            ptr++;
            firstHit = true; //This makes it so that the first building returned
                             // Is the only one that gets deleted
        }
    }
}
/////////////////////////////////////////////////////////////
// Callback Function called when right click  key is pressed. //
/////////////////////////////////////////////////////////////
void CallBackClickPressed(int button, int state, int x, int y)
{

    GLuint selectBuf[SIZE]; // Selection buffer, which is an array of size 512.
    GLint hits;
    GLint viewport[4]; // Current viewport size. See below.


   if(button == GLUT_RIGHT_BUTTON)
   {
      if(state == GLUT_DOWN)
      {
	 lookx = 5;
	 looky = 5;
	 lookz = 5;

      }else{
	 lookx=robotPosition[0];
	 looky=robotPosition[1];
	 lookz=robotPosition[2]+5;
      }
   }
   if(button == GLUT_LEFT_BUTTON)
   {

      if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
      {
        glGetIntegerv(GL_VIEWPORT, viewport);
        glSelectBuffer(SIZE, selectBuf);
        glRenderMode(GL_SELECT);

        glInitNames(); //initialize name stack
        glPushName(0); //push a name on to it

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity(); //install fresh projection matrix

        printf("Viewport x: %d", viewport[3]-y);
        printf("Viewport[3] - y : %d\n", viewport[3]-y);
        gluPickMatrix((GLdouble) x, (GLdouble) (viewport[3]-y), 
                               1.0,  1.0,       viewport);
        
        // gluPerspective(45.0f, //fovy
        //               (GLfloat)Window_Width/(GLfloat)Window_Height, //aspect
        //                5.0f, //zNear
        //                -50.0f);//zFar

        gluPerspective(20.0f, //fovy
                      (GLfloat)viewport[3]/(GLfloat)viewport[2], //aspect
                       5.0f, //zNear
                       -50.0f);//zFar
        // glOrtho(-2.0, 2.0, -2.0, 2.0, -2.0, 2.0);
        
        // glOrtho (-10.0, 10.0,
        //           -10.0, 10.0,
        //           -10, 10.0);  // This also related how sensitive we want our picking to be.
        // glMatrixMode(GL_MODELVIEW);
        // glLoadIdentity();
        drawBuildings(GL_SELECT);

        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glutSwapBuffers();


        hits = glRenderMode(GL_RENDER);
        processHits (hits, selectBuf);

        glutPostRedisplay();

      }
 
   }
}


///////////////////////////////////////////////////////////////
// Callback routine executed whenever the window is resized. //
//////////////////////////////////////////////////////////////
void CallBackResizeScene(int Width, int Height)
{
   // Let's not core dump, no matter what.
   if (Height == 0)
      Height = 1;

   // glViewport(0, 0, Width, Height);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(45.0f,(GLfloat)Width/(GLfloat)Height,0.1f,100.0f);

   glMatrixMode(GL_MODELVIEW);

   Window_Width  = Width;
   Window_Height = Height;
}

////////////////////////////////////////////////////////
//   Setup your program before passing the control    //
//   to the main OpenGL event loop.                   //
////////////////////////////////////////////////////////
void MyInit(int Width, int Height) 
{
   // Color to clear color buffer to.
   glClearColor(0.1f, 0.1f, 0.1f, 0.0f);

   // Depth to clear depth buffer to; type of test.
   glClearDepth(1.0);
   glDepthFunc(GL_LESS);
   glDepthMask(GL_TRUE);
   glCullFace(GL_BACK);
   glFrontFace(GL_CCW);

   // Enables Smooth Color Shading; try GL_FLAT for (lack of) fun.
   glShadeModel(GL_SMOOTH);

   // Load up the correct perspective matrix; using a callback directly.
   CallBackResizeScene(Width,Height);
   glEnable(GL_DEPTH_TEST);
   
   srand(time(NULL));
   spawnBuildings();
}



///////////////////////////////////////////////////
// main() function.                              //
//   There are general steps in making           //
//   an OpenGL application.                      //
//   Inits OpenGL.                               //
//   Calls our own init function                 //
//   then passes control onto OpenGL.            //   
///////////////////////////////////////////////////
int main(int argc, char **argv)
{
   glutInit(&argc, argv);
   glEnable(GL_CULL_FACE);
   // To see OpenGL drawing, take out the GLUT_DOUBLE request.
   glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

   // The following is for window creation.
   // Set Window size
   glutInitWindowSize(Window_Width, Window_Height);
   // Create and Open a window with its title.
   Window_ID = glutCreateWindow(PROGRAM_TITLE);

   //initRotate(argc, argv);
   // Register and install the callback function to do the drawing. 
   glutDisplayFunc(&CallBackRenderScene);

   // If there's nothing to do, draw.
   glutIdleFunc(&CallBackRenderScene);

   // It's a good idea to know when our window's resized.
   glutReshapeFunc(&CallBackResizeScene);

   // Register and install the callback function for
   // Some keys and special keys.
   glutKeyboardFunc(&CallBackKeyPressed);
   glutMouseFunc(&CallBackClickPressed);
   glutSpecialFunc(&CallBackSpecialPressed);
  
   // OK, OpenGL's ready to go.  Let's call our own init function.
   MyInit(Window_Width, Window_Height);
  
   // Print out a bit of help dialog.
   printf("\n%s\n\n", PROGRAM_TITLE);
   printf("Print out some helpful information here.\n");
   printf("When you program, you can also use\n");
   printf("printf to do debugging.\n\n");

   // Above functions represents those you will do to set up your
   // application program.
   // Now pass off control to OpenGL.
   glutMainLoop(); 
   // Never returns.
   return 1; 
}
